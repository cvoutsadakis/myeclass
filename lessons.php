<?php 
	include("include/config.php");

	if(!$_SESSION['loggedIn'])
	{
		header("Location: login.php");
	}
?>

<html>
	<!--<link href="include/default.css" rel="stylesheet" type="text/css"> -->

	<?php 
		include("include/head.php");
		include("include/menu.php");

	$host = "localhost";
	$user = "root";
	$pass = "";
	$db = "mydb";

	$conn = mysqli_connect($host, $user, $pass, $db);

	if(!$conn)
	{
		die("failed to connect!");
	}

	$sql = "SELECT * FROM lessons";
	$result = mysqli_query($conn, $sql);
	$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
	
	print("<style> table, th, td {border: 1px solid black;} </style>");

	print("<table style=\"width:50%\"");
	print("<tr><th><u>Lesson</u></th><th><u>Teacher</u></th><th><u>Semester</u></th></tr>");
	while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
	{
		print("<tr> <th>". $row['lesson'] . "</th><th>". $row['teacher']. "</th><th>". $row['semester']. "</th></tr>");
	}
	print("</table>");
	?>

