<?php
	session_start();
	switch($_SERVER["SCRIPT_NAME"])
	{
		case "/login.php":
			$PAGE_TITLE = "e-class";
			break;
		case "/create_account.php":
			$PAGE_TITLE = "Account Creation";
			break;
		case "/grades.php":
			$PAGE_TITLE = $_SESSION['username'] . " - Grades";
			break;
		case "/profile.php":
			$PAGE_TITLE = $_SESSION['username'] . " - Home";
			break;
		case "/lessons.php":
			$PAGE_TITLE = $_SESSION['username'] . " - Lessons";
			break;
		default:
			$PAGE_TITLE = "???";
	}
?>