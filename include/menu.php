<div id="menu">
	<ul>
		<li><a href="grades.php" accesskey="1" title="">Grades</a></li>
		<li><a href="lessons.php" accesskey="2" title="">Lesson List</a></li>
		<li><a href="about.html" accesskey="3" title="">About</a></li>
		<li class="dropdown">
			<button class="dropbtn"> <?php print($_SESSION['username']); ?></button> 
			<div class="dropdown-content">
				<a href="login.php" accesskey="4" <?php $_SESSION['loggedIn'] = false;?>>Log out</a>
			</div>
		</li>
	</ul>
</div>