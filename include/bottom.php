<div id="wrapper1">
    <div id="featured" class="container">
        <div class="box1">
            <h2>About the platform</h2>
            <p>
                - Αποτελεί εργαλείο επίλυσης φοιτητικών αποριών και ανάρτησης ανακοινώσεων απο καθηγητές. <br /> 
                - Προσφέρει άμεση επικοινωνία μεταξύ φοιτητών αλλά και του καθηγητή κάθε μαθήματος. <br />
                - Λειτουργεί ως μέσο γρήγορης ενημέρωσης.
            </p>
        </div>
        <div class="box2">
            <h2>About its creation</h2>
            <p>
                Created by Voutsadakis Christos. <br />
                Based on a previous university project.
            </p>
        </div>
    </div>
</div>